# HOME_ALCAR_kraken

HOME-ALCAR training experiments to fit aligned cartularies from HOME-ALCAR database into Kraken HTR core.

# Definitions

Detected script types for each cartulary

| Cartulary | CodeName |Script_type  |
| ------ | ------ |------ |
| Cartulaire             de l’Hôtel-Dieu de Pontoise                               | Pontoise               | Textualis + Hybrida    |
| Cartulaire             de l'abbaye Notre-Dame de la Roche                        | Notre-Dame de la Roche | Cursiva antiquior                           |
| Cartulaire             de l’abbaye de Port-Royal                                 | Port-Royal 1           | Textualis                                   |
| Cartulaire             de l'abbaye de Port-Royal                                 | Port-Royal 2           | Textualis                                   |
| Cartulaire             du chapitre cathédral Notre-Dame de Chartres              | Chartres 1             | Textualis + Cursiva |
| Cartulaire             du chapitre cathédral Notre-Dame de Chartres              | Chartres 2             | Textualis                                   |
| Premier             cartulaire de l'abbaye Notre-Dame de Molesme                 | Molesme 1              | Praegothica                                 |
| Grand             Cartulaire de l'abbaye de Molesme                              | Molesme 2              | Textualis                                   |
| Cartulaire             de l'abbaye de Saint-Denis, cartulaire blanc              | Saint-Denis            | Textualis                                   |
| Cartulaire             de Charles II de Navarre (Stein 2706)                     | Navarre                | Cursiva                                     |
| Cartulaire             de l'abbaye Notre-Dame de Vauluisant                      | Vauluisant             | Textualis                                   |
| Cartulaire             des commanderies de Sommereux et de Neuilly-sous-Clermont | Sommereux              | Cursiva antiquior + Textualis               |
| Cartulaire             de l'abbaye Notre-Dame de Fervaques                       | Fervaques              | Textualis                                   |
| Cartulaire             de l'abbaye S.Nicaise de Reims                            | Saint-Nicaise          | Textualis                                   |
| Cartulaire             de l'abbaye Notre-Dame de Clairmarais                     | Clairmarais            | Semihybrida                                 |
| Cartulaire             de la seigneurie de Nesle                                 | Nesle                  | Mixed                                       |
| Cartulaire             de l'abbaye de Pontigny                                   | Pontigny               | Textualis + Cursiva antiquior |

references:
[Kestemont, M., Christlein, V., & Stutzmann, D. (2017). Artificial Paleography: Computational Approaches to Identifying Script Types in Medieval Manuscripts. Speculum, 92(S1), S86-S109.](https://hal.archives-ouvertes.fr/hal-01854939/document)

# Architecture

### Architecure 1: 


![alt text for screen readers](https://gitlab.com/magistermilitum/home_alcar_kraken/-/raw/main/images/kraken_arch_1.drawio.png)

hyper_params': {'pad': 16, 'freq': 1.0, 'batch_size': 1, 'lag': 5, 'min_delta': None, 'optimizer': 'Adam', 'lrate': 0.0001, 'momentum': 0.9, 'weight_decay': 0, 'schedule': 'reduceonplateau', 'normalization': None, 'normalize_whitespace': True, 'augment': False, 'step_size': 10, 'gamma': 0.1, 'rop_patience': 3, 'cos_t_max': 50}}

### Architecure 2:

# Training



training board accuracy on validation set

<p float="left">
<img src="https://gitlab.com/magistermilitum/home_alcar_kraken/-/raw/main/images/val_acc_G1.jpg" width="546" height="400"> <img src="https://gitlab.com/magistermilitum/home_alcar_kraken/-/raw/main/images/val_acc_G2.jpg" width="546" height="400">
</p>





# Experiments

Testing Group 1 (Cursiva) against Nesle cartulary unseen during training (124 images, 228591 characters)

Testing Group 2 (Textualis) against Chartres_2 cartulary unseen during training (144 images, 271398 characters)


- **val_acc** = accuracy on validation set during training
- **test_acc** = accuracy on corpus test (Nesle or Chartres_2) after training
- **cer** = test character error rate
- **wer** = test word error rate

| model_name | Content | arch |val_acc | test_acc |cer | wer | logs |
| ------ | ------ |------ |------ |------ |------ |------ |------ |
| group1_test_1 | Navarre, ND_Roche, Clairmarais |arch_1 | 92.50% | 75.99% |22.68% |58.79% |[log_1](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G1_test_1_evaluation) |
| group1_test_2 | Navarre, ND_Roche, Clairmarais, +Morchesne, |arch_1 | 93.47% | 76.33% |22.52% | 58.21% |[log_2](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G1_test_2_evaluation) |
| group1_test_3 | Navarre, ND_Roche, Clairmarais, Sommereux |arch_1 |93.43% |77.32% |20.46% |54.34% | [log_3](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G1_test_3_evaluation) |
| group1_test_4 | Navarre, ND_Roche, Clairmarais, +Morchesne, Sommereux, +10 pages Nesle (fine-tuning) |arch_1| 93.19% |86.74% |12.92% |37.01% |[log_4](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G1_test_4_evaluation) |
| group1_test_5 | Navarre, ND_Roche, Clairmarais, +Morchesne, + e-NDP | arch_1 |94.22%| 82.24% |17.38% |48.61% |[log_5](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G1_test_5_evaluation) |
||[all G1 test metrics](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G1_all_tests_metrics.txt)|
| group2_test_1 | S_Denis, Fervaques, S_Nicaise, Port_Royal 1-2 |arch_1| 91.90% |87.39% |10.81% |29.20%  |[log_6](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G2_test_2b_evaluation) |
| group2_test_2 | S_Denis, Fervaques, S_Nicaise, Port_Royal 1-2, Vauluisant  |arch_1| 90.90% |88.17% |10.04% |26.63%  |[log_7](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G2_test_1b_evaluation) |
| group2_test_3 | S_Denis, Fervaques, S_Nicaise, Port_Royal 1-2, Vauluisant, Molesme_1  |arch_1| 92.76% |88.66% |9.64% |25.20%  |[log_8](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G2_test_3b_evaluation) |
| group2_test_3 | S_Denis, Fervaques, S_Nicaise, Port_Royal 1-2, Vauluisant, +10 pages Chartres_2 (fine-tuning)  |arch_1| 90.80% |89.20% |9.33% |24.83%  |[log_9] |
||[all G2 test metrics](https://gitlab.com/magistermilitum/home_alcar_kraken/-/blob/main/logs/G2_all_test_metrics.txt)|



